#ifndef ECONOMYAI_ENTITY_PROPERTIES_H
#define ECONOMYAI_ENTITY_PROPERTIES_H

#include <unordered_map>
#include "define.h"

namespace economyai
{
    enum class EntityType
    {
        Town = 0,
        City,
        Region,
        Count
    };
    
    enum class EntityResources
    {
        Oil = 0,
        Count
    };
    
    typedef std::unordered_map<EntityResources, int64> ResourcesMap;
    
    class EntityProperties
    {
    public:
        EntityProperties(const ResourcesMap& resources);
        EntityProperties();
        void setResourceValue(EntityResources resourceId, int64 resourceValue);
        int64 getResourceValue(EntityResources resourceId);
        
    private:
        ResourcesMap resources;
    };
}

#endif //ECONOMYAI_ENTITY_PROPERTIES_H
