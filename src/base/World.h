#ifndef ECONOMYAI_WORLD_H
#define ECONOMYAI_WORLD_H

#include "Country.h"

namespace economyai
{
    struct WorldSettings
    {
        std::string pathToData = "../../data";
        std::string pathToDefaultData = "../../data/default";
        std::string fileNameTerritorialEntitiesJSON = "TerritorialEntities.json";
    };
    
    class World
    {
    public:
        static World& getInstance();
        
        void init(const WorldSettings& settings = WorldSettings());
        bool update();
        
        void addCountry(const Country& country);
        
    private:
        World();
        ~World();
        World(World const&) = delete;
        World& operator= (World const&) = delete;
        
        void loadTerritorialEntities(std::string pathToFile);
        
        std::vector<Country> countries = std::vector<Country>();
        WorldSettings settings;
    };
}

#endif //ECONOMYAI_WORLD_H
