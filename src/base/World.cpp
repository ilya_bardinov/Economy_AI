#include "World.h"
#include <iostream>
#include <fstream>
#include "nlohmann/json.hpp"

namespace economyai
{
    World& World::getInstance()
    {
        static World world;
        return world;
    }
    
    World::World()
    {
        LOG_DEBUG("Create World");
    }
    
    World::~World()
    {
        LOG_DEBUG("Destroy World");
    }
    
    void World::init(const WorldSettings& _settings)
    {
        LOG_DEBUG("Initialize World");
        
        this->settings = _settings;
        
        loadTerritorialEntities(settings.pathToDefaultData + "/" + settings.fileNameTerritorialEntitiesJSON);
    }
    
    bool World::update()
    {
        for(Country& country : countries)
        {
            country.update();
        }
        
        return true;
    }
    
    void World::addCountry(const Country& country)
    {
        LOG_DEBUG("Add Country to World - %s", country.getName().c_str());
        countries.push_back(country);
    }
    
    void World::loadTerritorialEntities(std::string pathToFile)
    {
        LOG_DEBUG("Loading Territorial Entites data from JSON: %s", pathToFile.c_str());
        
        std::ifstream jsonFile(pathToFile);
        if (jsonFile.is_open())
        {
            LOG_DEBUG("JSON file was opened: %s", pathToFile.c_str());
        }
        else
        {
            LOG_ERROR("JSON file cannot be opened: %s", pathToFile.c_str());
            return;
        }
        
        nlohmann::json jsonData;
        jsonFile >> jsonData;
        
        if (jsonData.find("Countries") != jsonData.end())
        {
            jsonData = jsonData["Countries"];
            for (nlohmann::json::iterator it = jsonData.begin(); it != jsonData.end(); ++it) {
                std::string name = it.key();
                Country country(name);
                country.parseJSONData(it.value());
                addCountry(country);
            }
        }
    }
}
